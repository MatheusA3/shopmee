CREATE DATABASE shopmee;

CREATE TABLE visitante (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email varchar(255) NOT NULL,
    nome varchar(255) NOT NULL
);

SELECT * FROM visitante;