import { VisitanteService } from './../services/visitante.service';
import { Component, OnInit } from '@angular/core';
import { Visitante } from '../models/visitante.model';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import {ChangeDetectorRef} from '@angular/core'

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})

export class LandingPageComponent implements OnInit {

    visitantes: Visitante[] = [];
    visitante = new Visitante();
    erro = '';
    sucesso = '';

  constructor(
    private ref: ChangeDetectorRef,     
    private vService: VisitanteService,
    private route: ActivatedRoute,
    private router: Router,
    ) { }


  ngOnInit(){
    this.ref.detectChanges();
    window.scrollTo(0,20);
  }

  subscription(v) {
    this.vService.subscription(this.visitante)
      .subscribe(
        (res: Visitante[]) => {
          this.visitantes = res;
          this.sucesso = 'Visitante cadastro com sucesso.';
          v.reset();
        },
        (err) => this.erro = err
      );
  }

}
