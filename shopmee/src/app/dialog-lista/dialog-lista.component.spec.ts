import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogListaComponent } from './dialog-lista.component';

describe('DialogListaComponent', () => {
  let component: DialogListaComponent;
  let fixture: ComponentFixture<DialogListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
