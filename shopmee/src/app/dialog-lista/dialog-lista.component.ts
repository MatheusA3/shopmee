import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Visitante} from '../models/visitante.model';

@Component({
  selector: 'app-dialog-lista',
  templateUrl: './dialog-lista.component.html',
  styleUrls: ['./dialog-lista.component.css']
})
export class DialogListaComponent implements OnInit {
  form: FormGroup;
  description:string;
  visitante: Visitante = {email: '', nome:''};

  constructor(
    private dialogRef: MatDialogRef<DialogListaComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
     }

  ngOnInit(): void {

  }

  save() {
    this.dialogRef.close(this.visitante);
  }

  close() {
    this.dialogRef.close();
  }

}
