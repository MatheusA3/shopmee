import { Component, OnInit } from '@angular/core';
import {Injectable} from '@angular/core';
 
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
 
import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {Visitante} from '../models/visitante.model';

@Injectable({
    providedIn: 'root'
})
export class VisitanteService {
  baseUrl = 'http://localhost/services';
  visitantes: Visitante[] = [];

  constructor(private http: HttpClient) { }

  subscription(visitante: Visitante): Observable<Visitante[]> {
    console.log(visitante.email);
    console.log(visitante.nome);
    const dados = {
      email: visitante.email, nome: visitante.nome, 
    };
 
    return this.http.post(`${this.baseUrl}/gravar_visitante.php`, {dados: visitante})
      .pipe(map((res) => {
        // adiciona o novo aluno no vetor de alunos
        this.visitantes.push(res['dados']);
        // e o retorna para o chamador deste método
        return this.visitantes;
      }),
       
      catchError(this.tratarErro));
  }
 
  private tratarErro(error: HttpErrorResponse) {
    // vamos mostrar o erro no console
    console.log(error);
 
    // e vamos matar a aplicação aqui, pois não há mais nada
    // a fazer
    return throwError('Houve um erro: ' + error);
  }
}
