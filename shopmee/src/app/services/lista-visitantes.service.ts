import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';

import { map } from 'rxjs/operators';

import {Visitante} from '../models/visitante.model';


@Injectable({
  providedIn: 'root'
})
export class ListaVisitantesService {
  baseUrl = 'http://localhost/services';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(`${this.baseUrl}/get_visitante`).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }

  update(visitante: Visitante) {
    console.log("id no service:"+visitante.id);
    console.log("email no service:"+ visitante.email);
    console.log("nome no service:"+visitante.nome);
    return this.http.put(`${this.baseUrl}/update_visitante`, { data: visitante });
  }

  delete(id: any) {
    const params = new HttpParams()
      .set('id', id.toString());

    return this.http.delete(`${this.baseUrl}/delete_visitante`, { params: params });
  }

}
