import { TestBed } from '@angular/core/testing';

import { ListaVisitantesService } from './lista-visitantes.service';

describe('ListaVisitantesService', () => {
  let service: ListaVisitantesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListaVisitantesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
