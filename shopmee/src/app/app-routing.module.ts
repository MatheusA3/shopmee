import { ListaVisitantesComponent } from './lista-visitantes/lista-visitantes.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'landing-page', component: LandingPageComponent},
  { path: 'lista-visitantes', component: ListaVisitantesComponent},
  { path: '', redirectTo: '/landing-page', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
