import { DialogListaComponent } from './../dialog-lista/dialog-lista.component';
import { ListaVisitantesService } from './../services/lista-visitantes.service';
import { Component, OnInit, Inject } from '@angular/core';
import {Injectable} from '@angular/core';
 
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
 
import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {Visitante} from '../models/visitante.model';
import { ExportToCsv } from 'export-to-csv';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import { DialogConfirmacaoComponent } from '../dialog-confirmacao/dialog-confirmacao.component';

export interface DialogData {
  email: string;
  nome: string;
}

@Component({
  selector: 'app-lista-visitantes',
  templateUrl: './lista-visitantes.component.html',
  styleUrls: ['./lista-visitantes.component.css']
})

export class ListaVisitantesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nome', 'email', 'opcoes'];
  baseUrl = 'http://localhost/servicos';
  visitantes: Visitante[] = [];
  error = '';
  success = '';
  email: any;
  nome: any;
  visitante: Visitante = {email: '', nome:''};

  constructor(private http: HttpClient, private lvService: ListaVisitantesService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getVisitantes();
  }

  openDialog(id, email, nome) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {id: id, email: email, nome: nome}

    const dialogRef = this.dialog.open(DialogListaComponent, dialogConfig);
    
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        console.log('Realizando update.');
        this.visitante = result;
        this.updateVisitante(id);
      }else{
        console.log("Cancelamento da operação.");
      }
    });

  }

  confirmaDialog(id, email, nome){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {id: id, email: email, nome: nome}

    const dialogRef = this.dialog.open(DialogConfirmacaoComponent, dialogConfig);
    
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        console.log('Confirmado.');
        this.deleteVisitante(id);
      }else{
        console.log('Cancelado');
      }
    });
  }


  resetAlerts() {
    this.error = '';
    this.success = '';
  }
        
  getVisitantes(): void {
    this.lvService.getAll().subscribe(
      (data: Visitante[]) => {
        this.visitantes = data;
        this.success = 'successful retrieval of the list';
      },
      (err) => {
        console.log(err);
        this.error = err;
      }
    );
  }

  updateVisitante(id: any) {
    console.log("id:"+id);
    console.log("email:"+ this.visitante.email);
    console.log("nome:"+this.visitante.nome);

    this.resetAlerts();

    this.lvService
      .update({ id:+id, email: this.visitante.email, nome: this.visitante.nome })
      .subscribe(
        (res) => {
          this.success = 'Updated successfully';
          this.getVisitantes();
        },
        (err) => (this.error = err)
      );
  }

  deleteVisitante(id: number) {
    this.resetAlerts();

    this.lvService.delete(id).subscribe(
      (res) => {
        this.visitantes = this.visitantes.filter(function (item) {
          return item['id'] && +item['id'] !== +id;
        });

        this.success = 'Deleted successfully';
      },
      (err) => (this.error = err)
    );
  }

  exportar(){
      const options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true, 
        showTitle: true,
        title: 'listaVisitantes',
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
        // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
      };

      const csvExporter = new ExportToCsv(options);

      var csv = [];

      for(let v of this.visitantes){
        var id = v.id;
        var nome = v.nome;
        var email = v.email;

        csv.push({id, nome, email});
      }

      csvExporter.generateCsv(csv);
  }

}

